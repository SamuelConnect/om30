<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../public/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="" crossorigin="anonymous"> -->

    <link rel="stylesheet" href="/public/css/style.css">
    <title>KS Solution | Gestão de projetos</title>
</head>
<body>
    <div class="container-fluid container_fundo">    
        <div class="row">
            <div class="col-sm-4 col-xs-1 justify-content-center">

                <div class="col-sm-5 col-xs-1 text-left font-weight-bold text-info ">
                    bem-vindo(a)
                </div>
                <div class="col-sm-5 col-xs-1 text-left font-weight-bold text-white">
                    <p>Acompanhe seus projetos ativamente e participe de todo o processo.</p>
                </div>            
            </div>
            <div class="col-sm-3 col-xs-12 border p-5 m-5 rounded bg-white  alinhamento_central shadow-lg">
                <form action="/auth/login" method="post">
                    <div class="form-group">
                        <label for="InputEmail1">E-mail </label>
                        <input type="email" class="form-control" id="InputEmail1" aria-describedby="email" name="user_email" placeholder="Digite o e-mail cadastrado">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Senha</label>
                        <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Digite sua senha">
                    </div>
                    <div class="form-group form-check">
                    </div>
                    <div class="row"> 
                        <div class="col-12">
                            <button type="submit" class="btn  btn-block btn-info " id="botao_login">Acessar o sistema</button>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-1 btn-dark"></div>
                    </div> -->
                </form>
            </div>  
        </div> 
    </div>
</body>
</html>