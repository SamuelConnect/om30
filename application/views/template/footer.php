<footer class="main-footer">
    <strong>Copyright &copy; 2012 - <?php echo date('Y'); ?> <a href="http://kssolution.com.br">Samuel Valério</a>.</strong>
    Todos os direitos são reservados.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.0
    </div>
  </footer>
</div>