<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
  
    $this->load->view('template/header');
    $this->load->view('template/topbar');
    $this->load->view('template/sidebar');
    $this->load->view('template/content', $data); 
    $this->load->view('template/footer');
    $this->load->view('template/js');

?>