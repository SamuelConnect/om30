<!-- jQuery -->
<script src="/public/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="/public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="/public/dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="/public/plugins/chart.js/Chart.min.js"></script>
<script src="/public/dist/js/demo.js"></script>
<script src="/public/dist/js/pages/dashboard3.js"></script>
<script src="/public/js/valida_cep.js"></script>
</body>
</html>