<div class='container'>
    <form action="/paciente/upd" method="post">
        <h1> Atualizar paciente</h1>
        <div class="form-row"> 
                <input type="text" class="form-control" value= "<?php echo $data[0]->paciente_id;?>"
                name="paciente_id"  hidden  required>     
            <div class="form-group col-md-6">
                <label for="paciente_nome_completo">Nome completo*</label>
                <input type="text" class="form-control" value= "<?php echo $data[0]->paciente_nome_completo;?>"
                name="paciente_nome_completo" placeholder="Nome completo" required>
            </div>
            <div class="form-group col-md-6">
                <label for="paciente_nome_mae_completo">Nome completo da Mãe*</label>
                <input type="text" class="form-control"  name="paciente_nome_mae_completo" placeholder="Nome da mãe" value= "<?php echo  $data[0]->paciente_nome_mae_completo;?>" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="date">Data de nascimento*</label>
                <input type="date" class="form-control" name="paciente_nascimento" value= "<?php echo $data[0]->paciente_nascimento;?>"required>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAddress2">CPF*</label>
                <input type="text" class="form-control" name="paciente_cpf" value= "<?php echo $data[0]->paciente_cpf;?>" required>
            </div>
            <div class="form-group col-md-4">
                <label for="paciente_cns">CNS*</label>
                <input type="text" class="form-control" name="paciente_cns"value= "<?php echo $data[0]->paciente_cns;?>" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="paciente_cep">cep*</label>
                <input type="text" class="form-control" name="paciente_cep" id="paciente_cep" onblur="pesquisacep(this.value)" value= "<?php echo $data[0]->paciente_cep;?>" required>
            </div>
            <div class="form-group col-md-6">
                <label for="paciente_rua">Rua*</label>
                <input type="text" class="form-control" name="paciente_rua" id="paciente_rua" value="<?php echo $data[0]->paciente_rua;?>" required>
            </div>
            <div class="form-group col-md-2">
                <label for="paciente_numero">Número*</label>
                <input type="text" class="form-control" name="paciente_numero" id="paciente_numero"value= "<?php echo $data[0]->paciente_numero;?>" required>
            </div>
            <div class="form-group col-md-6">
                <label for="paciente_bairro">Bairro*</label>
                <input type="text" class="form-control" name="paciente_bairro"  id="paciente_bairro"value= "<?php echo $data[0]->paciente_bairro;?>" required>
            </div>
            
            <div class="form-group col-md-6">
                <label for="inputCity">Cidade*</label>
                <input type="text" class="form-control" name="paciente_cidade" id="paciente_cidade" value= "<?php echo $data[0]->paciente_cidade;?>"required>
            </div>
            <div class="form-group col-md-6">
                <label for="inputCity">UF*</label>
                <input type="text" class="form-control" name="paciente_estado" id="paciente_uf"  value= "<?php echo $data[0]->paciente_uf;?>" required>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Atualizar paciente</button>
    </form>
    <div class="row">
        <div class="col-12 mt-5">
            <p><strong>Os campos marcados com * tem o preenchimento como obrigatórios.<strong></p>
        </div>
    </div>
</div>