<div class='container'>
    <form action="/paciente/add" method="post">
        <h1> Adicionar paciente</h1>
        <div class="form-row">

            <div class="form-group col-md-6">
                <label for="paciente_nome_completo">Nome completo*</label>
                <input type="text" class="form-control" name="paciente_nome_completo" placeholder="Nome completo" required>
            </div>
            <div class="form-group col-md-6">
                <label for="paciente_nome_mae_completo">Nome completo da Mãe*</label>
                <input type="text" class="form-control" name="paciente_nome_mae_completo" placeholder="Nome da mãe" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="date">Data de nascimento*</label>
                <input type="date" class="form-control" name="paciente_nascimento" required>
            </div>
            <div class="form-group col-md-4">
                <label for="inputAddress2">CPF*</label>
                <input type="text" class="form-control" name="paciente_cpf" placeholder="333.333.333-92" required>
            </div>
            <div class="form-group col-md-4">
                <label for="paciente_cns">CNS*</label>
                <input type="text" class="form-control" name="paciente_cns" placeholder="333333333333333" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="paciente_cep">cep*</label>
                <input type="text"  class="form-control" onblur="pesquisacep(this.value);" name="paciente_cep" id="paciente_cep" placeholder="33.333-333" required>
            </div>
            <div class="form-group col-md-6">
                <label for="paciente_rua">Rua*</label>
                <input type="text" class="form-control" name="paciente_rua" id="paciente_rua" placeholder="Rua das couves" required>
            </div>
            <div class="form-group col-md-2">
                <label for="paciente_numero">Número*</label>
                <input type="text" class="form-control" name="paciente_numero" id="paciente_numero" placeholder="333" required>
            </div>
            <div class="form-group col-md-6">
                <label for="paciente_bairro">Bairro*</label>
                <input type="text" class="form-control" name="paciente_bairro" id="paciente_bairro" placeholder="Tatuapé" required>
            </div>
            
            <div class="form-group col-md-6">
                <label for="inputCity">Cidade*</label>
                <input type="text" class="form-control" id="paciente_cidade" name="paciente_cidade" placeholder="São Paulo" required>
            </div>
            <div class="form-group col-md-6">
                <label for="inputCity">UF*</label>
                <input type="text" class="form-control" id="paciente_uf" name="paciente_estado" placeholder="SP" required>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Adcionar paciente</button>
    </form>
    <div class="row">
        <div class="col-12 mt-5">
            <p><strong>Os campos marcados com * tem o preenchimento como obrigatórios.<strong></p>
        </div>
    </div>
</div>