
<body>   
    <!-- <div class="container"> -->
        <div class="row">
            <div class="col-sm-11 col-xs-12 ml-4 mt-4 ">  
                <h3 class="text-dark ml-2">Lista de pacientes</h3>
                <div class="col-sm-1">
                    <a href="<?php base_url();?>/paciente/add/"class="nav-link">
                        <div class=" text-right fa fa-plus nav-icon"></div>
                    </a>
                </div>
                <div class="col-sm-11">
                    <table class="table table-striped   shadow-lg text-dark">
                        <thead>
                            <tr>
                                <th scope="col">Status</th>
                                <th scope="col">Nome</th>
                                <th scope="col">CPF</th>
                                <th scope="col">CNS</th>
                                <!-- <th scope="col">Empresa</th> -->
                                <th scope='col'>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data as $chave => $paciente) {?>
                                <tr>
                                     <?php if($chave !== 'view'){?>
                                        <th scope="row"> <?php echo $paciente->paciente_status; ?></th>
                                        <td><?php echo $paciente->paciente_nome_completo;?></td>
                                        <td><?php echo $paciente->paciente_cpf;?></td>
                                        <td><?php echo $paciente->paciente_cns;?></td>
                                        <!-- <td><?php echo $project->project_company;?></td> -->
                                        <td>
                                            <!-- ver/upd -->
                                            <a href="/paciente/upd/<?php echo $paciente->paciente_id;?>" type="button" class="btn  btn-sm btn-success far fa-edit nav-icon"></a>
                                            <!-- deletar -->
                                            <a href="/paciente/delet/<?php echo $paciente->paciente_id;?>" type="button" class="btn btn-sm btn-danger fa fa-window-close nav-icon" alt="deletar paciente"></a> 
                                        </td>
                                    <?php }?>
                                </tr>       
                            <?php }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <!-- </div> -->
</body>

</html>



