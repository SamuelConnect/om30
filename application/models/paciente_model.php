<?php 
class paciente_model extends CI_Model{
 private $name = 'paciente';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function add($data)
    {
        $query = $this->db->insert('paciente', $data);
        return $query;
    }

    public function getAll()
    {
        $select = $this->db->list_fields($this->name);
        $this->db->from($this->name);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function getById($paciente_id)
    {
        $select = $this->db->list_fields($this->name);
        $this->db->select($select);
        $this->db->from($this->name);
         $this->db->where('paciente_id', $paciente_id );        
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    public function delete($paciente_id)
    {
        $this->db->where('paciente_id', $paciente_id);
        return $this->db->delete($this->name);
    }
    public function upd($paciente_id, $data)
    {
        $this->db->set($data);
        $this->db->where('paciente_id', $paciente_id);

        $res = $this->db->update($this->name);
        if($res == true){
            return true;
        }else{
            return false;
        }
    }
}
