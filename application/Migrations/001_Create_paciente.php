<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_paciente extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'paciente_id' => array(
                            'type'           => 'INT',
                            'constraint'     => 5,
                            'unsigned'       => TRUE,
                            'auto_increment' => TRUE,
                            'unique'         => TRUE
                        ),
                        'paciente_nome_completo' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '255',
                        ),
                        'paciente_nome_mae_completo' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '255',
                        ),
                        'paciente_nascimento' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '11',
                        ),
                        'paciente_cpf' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '11',
                        ),
                        'paciente_cns' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '15',
                        ),

                        'paciente_cep' => array(
                            'type' => 'CHAR',
                            'constraint' => '8',
                        ),

                        'paciente_numero' => array(
                            'type' => 'CHAR',
                            'constraint' => '8',
                        ),

                        'paciente_rua' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '100',
                        ),

                        'paciente_bairro' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '25',
                        ),

                        'paciente_cidade' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '25',
                        ),

                        'paciente_uf' => array(
                            'type' => 'CHAR',
                            'constraint' => '2',
                        ),
                        'paciente_status' => array(
                            'type' => 'CHAR',
                            'constraint' => '2',
                        ),
                        'paciente_inclusao' => array(
                            'type' => ' VARCHAR',
                            'constraint' => '10',
                        ),  
                ));

                $this->dbforge->create_table('paciente');
        }

        public function down()
        {
                $this->dbforge->drop_table('paciente');
        }
}
?>