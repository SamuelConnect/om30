<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 class auth extends CI_Controller{
    function __construct(){
		parent::__construct();
		$this->load->model('auth_model');
	}
	
	public function index(){
        $this->load->library('session');
        if($this->session->userdata('login') == 'on'){
            redirect('/project/get');
        }else{
            redirect('/auth/login');
        }
    }
 
    public function login(){
        if($this->input->post()){
			$user_email 		= $this->input->post('user_email');
			$user_password 		= $this->input->post('user_password');
			
			$user_email     = str_replace( " ","", $user_email);
			$user_password  = str_replace(" ", "", $user_password); 
			if(empty($user_email) || empty($user_password)){
				$data = array(
					"error" 	=> "Invalid email address or password, check out and try again.",
					"content"	=> ""
				);
			}
			$result = $this->auth_model->getLogInByEmail($user_email,['user_id','user_password', 'user_name','user_email', 'user_company' ,'user_phone','user_status'], $user_password); 

			if($result === true){
				 redirect('/project/get');
			}else{
				redirect('/auth/login');
			}
		}else{
			$this->load->view('login/home');
		}
		
	}

}
?>