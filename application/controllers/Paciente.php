<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 class Paciente extends CI_Controller{
    function __construct(){
		parent::__construct();
		$this->load->model('paciente_model');
	}
	

	public function add(){
    	if($this->input->post()){

    	 	if(empty($_POST["paciente_nome_completo"]) || !isset($_POST["paciente_nome_completo"])){
    	 		die('Campo nome completo não foi preenchido corretamente, verifique e tente novamente.');
    	 	}
    	 	if(empty($_POST["paciente_nome_mae_completo"]) || !isset($_POST['paciente_nome_mae_completo'])){
    	 		die('Campo nome da mãe não foi preenchido corretamente, verifique e tente novamente.');
    	 	} 
    	 	if(empty($_POST["paciente_nascimento"]) || !isset($_POST["paciente_nascimento"])){
    	 		die('Campo Data de nascimento não foi preenchido corretamente, verifique e tente novamente.');
    	 	}  
    	 	if($_POST["paciente_cpf"]){
    	 		$this->load->helper('My_cpf_helper');
                $resp = validaCPF($_POST['paciente_cpf']);
                if($resp == false){
                    die('cpf informado não é valido');
                }else{
                    $cpf = $_POST["paciente_cpf"];
                }
    	 	} 

            if($_POST["paciente_cns"]){
                $this->load->helper('My_cns_helper');
                $res = validaCNS($_POST['paciente_cns']);
                if($res == false){ 
                    die('Campo CNS não foi preenchido corretamente, verifique e tente novamente.');

                }

            }
            $cep = str_replace('-', '', $_POST['paciente_cep']);
            if(empty($_POST["paciente_cep"]) || !isset($_POST["paciente_cep"])){

                die('Campo CEP não foi preenchido corretamente, verifique e tente novamente.');
            } 
               if(empty($_POST["paciente_rua"]) || !isset($_POST["paciente_rua"])){
                die('Campo Rua não foi preenchido corretamente, verifique e tente novamente.');
            } 
                if(empty($_POST["paciente_numero"]) || !isset($_POST["paciente_numero"])){
                die('Campo Número não foi preenchido corretamente, verifique e tente novamente.');
            } 
              if(empty($_POST["paciente_cidade"]) || !isset($_POST["paciente_cidade"])){
                die('Campo Cidade não foi preenchido corretamente, verifique e tente novamente.');
            } 
              if(empty($_POST["paciente_estado"]) || !isset($_POST["paciente_cidade"])){
                die('Campo UF não foi preenchido corretamente, verifique e tente novamente.');
            } 
            if(empty($_POST["paciente_bairro"]) || !isset($_POST["paciente_bairro"])){
                die('Campo Bairro não foi preenchido corretamente, verifique e tente novamente.');
            } 

            $dados = [

                "paciente_nome_completo"        =>  $_POST["paciente_nome_completo"],
                "paciente_nome_mae_completo"    =>  $_POST["paciente_nome_mae_completo"],
                "paciente_nascimento"           =>  $_POST["paciente_nascimento"],
                "paciente_cpf"                  =>  $cpf,
                "paciente_cns"                  =>  $_POST["paciente_cns"],
                "paciente_cep"                  =>  $cep,
                "paciente_rua"                  =>  $_POST["paciente_rua"],
                "paciente_bairro"               =>  $_POST["paciente_bairro"],
                "paciente_numero"               =>  $_POST['paciente_numero'],
                "paciente_cidade"               =>  $_POST["paciente_cidade"],
                "paciente_uf"                   =>  $_POST["paciente_estado"],
                'paciente_status'               =>  1,
                // "paciente_foto"                 =>  $_POST["paciente_foto"]
                "paciente_inclusao"             =>time()
            ];
           
            $resp = $this->paciente_model->add($dados);
            if($resp == true){
                redirect('/paciente/get');
            }

    	}//post
    	
        $data['view'] = array('view'  => 'paciente/add');
        $data['data'] = $data;
        $this->load->view('template/config', $data);

    } 

	public function get()
	{
		$data = $this->paciente_model->getAll();
		
        $data['view'] = array('view'  => 'paciente/list');
        $data['data'] = $data;

        $this->load->view('template/config', $data);

	}
    public function delet()
    {
        $paciente_id = $this->uri->segment(3);
        $resp = $this->paciente_model->delete($paciente_id);
        if($resp == true){
            echo "Paciente apagado com sucesso.";
            redirect('/paciente/get');
        }
    }
    public function upd()
    {
        $paciente_id = $this->uri->segment(3);
        $data = $this->paciente_model->getById($paciente_id);
        if($this->input->post()){

            if(empty($_POST["paciente_nome_completo"]) || !isset($_POST["paciente_nome_completo"])){
                die('Campo nome completo não foi preenchido corretamente, verifique e tente novamente.');
            }
            if(empty($_POST["paciente_nome_mae_completo"]) || !isset($_POST['paciente_nome_mae_completo'])){
                die('Campo nome da mãe não foi preenchido corretamente, verifique e tente novamente.');
            } 
            if(empty($_POST["paciente_nascimento"]) || !isset($_POST["paciente_nascimento"])){
                die('Campo Data de nascimento não foi preenchido corretamente, verifique e tente novamente.');
            }  
            if($_POST["paciente_cpf"]){
                $this->load->helper('My_cpf_helper');
                $resp = validaCPF($_POST['paciente_cpf']);
                if($resp == false){
                    die('cpf informado não é valido');
                }else{
                    $cpf = $_POST["paciente_cpf"];
                }
            } 
            
            if($_POST["paciente_cns"]){
                $this->load->helper('My_cns_helper');
                $res = validaCNS($_POST['paciente_cns']);
                if($res == false){ 
                    die('Campo CNS não foi preenchido corretamente, verifique e tente novamente.');

                }
            }
            $cep = str_replace('-', '', $_POST['paciente_cep']);
            if(empty($_POST["paciente_cep"]) || !isset($_POST["paciente_cep"])){

                die('Campo CEP não foi preenchido corretamente, verifique e tente novamente.');
            } 
               if(empty($_POST["paciente_rua"]) || !isset($_POST["paciente_rua"])){
                die('Campo Rua não foi preenchido corretamente, verifique e tente novamente.');
            } 
                if(empty($_POST["paciente_numero"]) || !isset($_POST["paciente_numero"])){
                die('Campo Número não foi preenchido corretamente, verifique e tente novamente.');
            } 
              if(empty($_POST["paciente_cidade"]) || !isset($_POST["paciente_cidade"])){
                die('Campo Cidade não foi preenchido corretamente, verifique e tente novamente.');
            } 
              if(empty($_POST["paciente_estado"]) || !isset($_POST["paciente_cidade"])){
                die('Campo UF não foi preenchido corretamente, verifique e tente novamente.');
            } 
            if(empty($_POST["paciente_bairro"]) || !isset($_POST["paciente_bairro"])){
                die('Campo Bairro não foi preenchido corretamente, verifique e tente novamente.');
            } 

            $dados = [

                "paciente_nome_completo"        =>  $_POST["paciente_nome_completo"],
                "paciente_nome_mae_completo"    =>  $_POST["paciente_nome_mae_completo"],
                "paciente_nascimento"           =>  $_POST["paciente_nascimento"],
                "paciente_cpf"                  =>  $cpf,
                "paciente_cns"                  =>  $_POST["paciente_cns"],
                "paciente_cep"                  =>  $cep,
                "paciente_rua"                  =>  $_POST["paciente_rua"],
                "paciente_bairro"               =>  $_POST["paciente_bairro"],
                "paciente_numero"               =>  $_POST['paciente_numero'],
                "paciente_cidade"               =>  $_POST["paciente_cidade"],
                "paciente_uf"                   =>  $_POST["paciente_estado"],
                'paciente_status'               =>  1,
                // "paciente_foto"                 =>  $_POST["paciente_foto"]
                "paciente_inclusao"             =>time()
            ];

            $paciente_id = $_POST['paciente_id'];
            $resp = $this->paciente_model->upd($paciente_id, $dados);
            if($resp == true){
                redirect('/paciente/get');
            }

        }
        $data['view'] = array('view'  => 'paciente/upd');
        $data['data'] = $data;
        $this->load->view('template/config', $data);
    }

}